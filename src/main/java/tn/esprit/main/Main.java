/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import tn.esprit.conf.AppConfig;
import tn.esprit.exception.VersementExceptionChecked;
import tn.esprit.exception.VersementExceptionUnchecked;
import tn.esprit.service.TransfertSoldeService;
/**
 * 
 * @author Walid YAICH
 */
public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		TransfertSoldeService transfertService = (TransfertSoldeService) ctx.getBean("transfertService");
		
//		try{
//			transfertService.transfert(1l, 2l, 50l);	
//		}catch (VersementExceptionUnchecked versEx) {
//			logger.error("Versement echoué, veuillez vérifier l'integrité des données", versEx);
//		}
		
		
		try{
			transfertService.transfertPropagation(1l, 2l, 50l);	
		}catch (VersementExceptionChecked versEx) {
			logger.error("Versement echoué, veuillez vérifier l'integrité des données", versEx);
		}
	}
}

