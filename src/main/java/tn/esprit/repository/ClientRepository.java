/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.exception.VersementExceptionChecked;
import tn.esprit.exception.VersementExceptionUnchecked;

@Repository
public class ClientRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public void versement(Long idClient, Long montant) throws VersementExceptionChecked{
		Long ancienSolde = jdbcTemplate.queryForObject("select solde from Client where id=?", Long.class, idClient);
		Long nouveauSolde = ancienSolde + montant;
		/*******POUR SIMULER UNE ERREUR AU COURS DU VERSEMENT*************************/
		if(nouveauSolde > 0){
			throw new VersementExceptionChecked();			
		}
		/**************************************************************************/
		jdbcTemplate.update("update Client SET solde=? where id=?", nouveauSolde, idClient);
	}
	
	
	//@Transactional(propagation=Propagation.REQUIRES_NEW) //Une nouvelle transaction va etre ouverte et fermé dans cette méthode, 
	//a la sortie de cette méthode, pas de chance pour faire le rollback
	public void retrait(Long idClient, Long montant){
		Long ancienSolde = jdbcTemplate.queryForObject("select solde from Client where id=?", Long.class, idClient);
		Long nouveauSolde = ancienSolde - montant;
		jdbcTemplate.update("update Client SET solde=? where id=?", nouveauSolde, idClient);
	}
	
}
