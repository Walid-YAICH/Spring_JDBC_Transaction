/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.exception;

public class VersementExceptionUnchecked extends RuntimeException {

	private static final long serialVersionUID = 1042859085471770626L;

}
