/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.exception.VersementExceptionChecked;
import tn.esprit.exception.VersementExceptionUnchecked;
import tn.esprit.repository.ClientRepository;

@Service("transfertService")
public class TransfertSoldeService {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ClientRepository clientRepo;
	
	/**
	 * Cette méthode permet de transférer un montant x d'un client a un autre.
	 * @param idEmetteur
	 * @param idBeneficiaire
	 * @param montant
	 */
	@Transactional
	public void transfert(Long idEmetteur, Long idBeneficiaire, Long montant){
		//Retrait
		Long ancienSolde = jdbcTemplate.queryForObject("select solde from Client where id=?", Long.class, idEmetteur);
		Long nouveauSolde = ancienSolde - montant;
		jdbcTemplate.update("update Client SET solde=? where id=?", nouveauSolde, idEmetteur);
		
		//Versement
		ancienSolde = jdbcTemplate.queryForObject("select solde from Client where id=?", Long.class, idBeneficiaire);
		nouveauSolde = ancienSolde + montant;
		/*******POUR SIMULER UNE ERREUR AU COURS DU VERSEMENT*************************/
		if(nouveauSolde > 0){
			throw new VersementExceptionUnchecked();			
		}
		/**************************************************************************/
		jdbcTemplate.update("update Client SET solde=? where id=?", nouveauSolde, idBeneficiaire);
	}
	
	@Transactional(rollbackFor=VersementExceptionChecked.class)
	public void transfertPropagation(Long idEmetteur, Long idBeneficiaire, Long montant) throws VersementExceptionChecked{
		clientRepo.retrait(idEmetteur, montant);
		clientRepo.versement(idBeneficiaire, montant);
	}
	
}
